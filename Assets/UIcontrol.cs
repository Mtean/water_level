﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIcontrol : MonoBehaviour
{
    public HeadWatersPanel H;
    public GameObject prafab;
    public Transform[] points;
    public Text txt1;
    public Text txt2;
    public Text txt3;
    void Start()
    {
        for (int i = 0; i < points.Length; i++)
        {
            GameObject go = Instantiate<GameObject>(prafab,transform);
            
            go.GetComponent<BindToPoint>().InitUI(points[i],H);
            
            go.SetActive(true);

        }
    }
    public void showTxt(int num1, int num2, int num3)
    {
        txt1.text = num1.ToString();
        txt2.text = num2.ToString();
        txt3.text = num3.ToString();
    }
}
