﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 地图信息集控制器
/// </summary>
public class MapDatasController : MonoBehaviour
{
    /// <summary>
    /// 静态实例化
    /// </summary>
    public static MapDatasController _Instance;
	/// <summary>
	/// 数据点集合
	/// </summary>
	public List<CityControllerPlane> CityPoints;
    public InputField input;
    public ContentSizeFitter content;
    public void Awake()
    {
        _Instance = this;
        input.onValueChanged.AddListener(OnValueChanged);
    }
    /// <summary>
    /// 获取HTML输入字段
    /// </summary>
    public void CallText()
    {
        Application.ExternalCall("mySearch", gameObject.name);
    }
    public void GetcompileText(string x)
    {
        Debug.Log(x + "------------");
        //SearchName.text = x;
    }

    /// <summary>
    /// 点位添加
    /// </summary>
    public void OnAddPoint(MapPointData data)
    {
        for (int i = 0; i < CityPoints.Count; i++)
        {
            if (data.Pointdata.county == CityPoints[i].cityWater.type.ToString())
            {
                CityPoints[i].AddMapPoint(data);
                continue;
            }
        }
    }
    /// <summary>
    /// 搜索
    /// </summary>
    public void OnValueChanged(string name)
    {
        if (string.IsNullOrEmpty(name))
        {
            content.enabled = false;
            for (int i = 0; i < CityPoints.Count; i++)
                CityPoints[i].OnRestChangedShow();
           content.SetLayoutVertical();
           content.enabled = true;
            return;
        }
        for (int i = 0; i < CityPoints.Count; i++)
        {
            CityPoints[i].OnSearch(name);
        }
    }
    /// <summary>
    /// 初始化
    /// </summary>
    public void OnInit()
    {

    }
    /// <summary>
    /// 关闭
    /// </summary>
    public void OnClose()
    {
        for (int i = 0; i < CityPoints.Count; i++)
        {
            CityPoints[i].OnClose();
        }
    }
    /// <summary>
    /// 切换选中
    /// </summary>
    public void ChangeToole(int num)
    {
        for (int i = 0; i < CityPoints.Count; i++)
        {
            if (i==num)
            {
                CityPoints[i].OnInit();
                continue;
            }
                CityPoints[i].OnClose();
        }
    }
}
