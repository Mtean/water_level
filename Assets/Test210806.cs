﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test210806 : MonoBehaviour
{
    public GameObject game;
    float m;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector2.Lerp(transform.position, game.transform.position, Time.deltaTime);
        m = (transform.position - game.transform.position).magnitude;
        Debug.Log(m);
    }
}
