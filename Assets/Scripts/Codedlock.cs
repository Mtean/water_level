﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Codedlock : MonoBehaviour
{
    public Image image;
    // Start is called before the first frame update
    void Start()
    {
        DateTime MaxDate = Convert.ToDateTime("2021-11-15");
        DateTime NowTime=DateTime.Now.ToLocalTime();
        if (NowTime.CompareTo(MaxDate)>=0)
            image.gameObject.SetActive(true);
        else
            image.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
