﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
public class ScrollGridHorizontalTest : MonoBehaviour
{
    public GameObject tempCell;
    public int GridCount;
    void Start()
    {
        InitScroll(GridCount);
    }

   
    private void InitScroll(int GridCount)
    {
        ScrollGridHorizontal scrollGridVertical = gameObject.AddComponent<ScrollGridHorizontal>();
        scrollGridVertical.tempCell = tempCell;
        scrollGridVertical.AddCellListener(this.OnCellUpdate);
        scrollGridVertical.SetCellCount(GridCount);
    } 
    private void OnCellUpdate(ScrollGridCell cell)
    {
        cell.gameObject.GetComponentInChildren<Text>().text = cell.index.ToString();
    }
}
