﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/// <summary>
/// 动画树信息面板
/// </summary>
public class WaterControllerPlane : MonoBehaviour
{
	/// <summary>
	/// 基本信息
	/// </summary>
	public Text BasicInfo;
	/// <summary>
	/// 所在河段
	/// </summary>
	public Text reach;
	/// <summary>
	/// 水源类型
	/// </summary>
	public Text waterType;
	/// <summary>
	/// 集水面积
	/// </summary>
	public Text hydropsPlane;
	/// <summary>
	/// 取水口高程
	/// </summary>
	public Text Intake;
	/// <summary>
	/// 设计供水模式
	/// </summary>
	public Text DesignWaterScale;
	/// <summary>
	/// 日取水量
	/// </summary>
	public Text dayIntake;
	/// <summary>
	/// 供水人数
	/// </summary>
	public Text waterPeopleNum;
	/// <summary>
	/// 运行管理单位
	/// </summary>
	public Text ManagementUnit;
	/// <summary>
	/// 初始化
	/// </summary>
	public void OnInitAni(Pointdata data)
    {
		OnDataInitPlane(data);
	}
	/// <summary>
	/// 信息面板初始化
	/// </summary>
	public void OnDataInitPlane(Pointdata data)
    {
		BasicInfo.text = data.basicInfo;
		reach.text = data.reach;
		ManagementUnit.text = data.managementUnit;
		hydropsPlane.text = data.hydropsPlane;
		Intake.text = data.intake+"米";
		dayIntake.text = data.dayIntake;
		waterPeopleNum.text = data.waterPeopleNum+"人";
		waterType.text = data.waterType;
		DesignWaterScale.text = data.designWaterScale+"吨/日";
    }
}
