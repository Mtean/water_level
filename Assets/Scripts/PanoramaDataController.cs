﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/// <summary>
/// 全景信息控制器
/// </summary>
public class PanoramaDataController : MonoBehaviour
{
    /// <summary>
    /// 标识
    /// </summary>
    public string Ide;
    /// <summary>
    /// 数据
    /// </summary>
    public PanoramaData data;
    /// <summary>
    /// 取水高程
    /// </summary>
    public Text ElevationPlane;
    /// <summary>
    /// 底板高程
    /// </summary>
    public Text BaseboardPlane;
    /// <summary>
    /// 水位
    /// </summary>
    public Text WPlane;
    /// <summary>
    /// 容量
    /// </summary>
    public Text ZPlane;
    /// <summary>
    /// 可用天数
    /// </summary>
    public Text UsableWaterPlane;
    /// <summary>
    /// 数据输入
    /// </summary>
    public void OnSetData(PanoramaData panorama)
    {
        data = panorama;
        //WPlane.text = data.Z != "" ? data.Z + "米" : "";
        //ZPlane.text = data.W != "" ? data.W + "米" : "";
    }
    /// <summary>
    /// 显示控制
    /// </summary>
    public void OnIsShow(bool isShow)
    {
        gameObject.SetActive(isShow);
    }
}
