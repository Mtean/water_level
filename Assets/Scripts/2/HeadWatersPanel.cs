﻿using LitJson;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class PlaceData
{
    /// <summary>
    /// 取水口底板高程
    /// </summary>
    public string qushuikoudibangaocheng;

    /// <summary>
    /// 分层取水口高程
    /// </summary>
    public string fencenggaocheng;

    /// <summary>
    /// 实时水位
    /// </summary>
    public string shishishuiwei;

    /// <summary>
    /// 库容
    /// </summary>
    public string kurong;

    /// <summary>
    /// 死库容
    /// </summary>
    public string sikurong;

    /// <summary>
    /// 当前可供水量
    /// </summary>
    public string dangqiankegongshuiliang;

    /// <summary>
    /// 日供水量
    /// </summary>
    public string rigongshuiliang;

    /// <summary>
    /// 日灌溉水量
    /// </summary>
    public string riguanggaishuiliang;

    /// <summary>
    /// 生态下泄流量
    /// </summary>
    public string shengtaixiaxieliuliang;

    /// <summary>
    /// 入库流量
    /// </summary>
    public string rukuliuliang;

    /// <summary>
    /// 3日后可供水量
    /// </summary>
    public string sanrihou;

    /// <summary>
    /// 6日后可供水量
    /// </summary>
    public string liurihou;

    /// <summary>
    /// 9日后可供水量
    /// </summary>
    public string jiurihou;

    /// <summary>
    /// 建议
    /// </summary>
    public string jianyi;

}



[Serializable]
public class CityPlace
{
    [SerializeField]
    public string City;
    [SerializeField]
    public List<Transform> place = new List<Transform>();
}


public class HeadWatersPanel : MonoBehaviour
{
    public Mouselook MouseL;
    private int GridCount;

    public Transform Content;
    public GameObject Item;

    public GameObject exclItem;
    [SerializeField]
    public List<CityPlace> List_CityPlace;
    private Dictionary<string, string> Dic_PlaceID = new Dictionary<string, string>();
    public Dictionary<string, List<string>> Dic_CityPlace = new Dictionary<string, List<string>>();
    public Dictionary<string, PlaceData> Dic_PlaceInfo = new Dictionary<string, PlaceData>();
    public Dictionary<string, GameObject> togglesAll = new Dictionary<string, GameObject>();
    public Text TitleText;
    private Transform lastShowPanorama;
    private Toggle lastShowPanoramaToggle;

    public Transform Map;
    public Transform infoPanel;
    public Transform excPanelScrollView;
    public Transform panelExc;
    public Animator aniTor;

    private Toggle defaultToggle0;
    private Toggle defaultToggle;
    JsonData jdExcel = new JsonData();
    public GameObject pointsUI;
    public Toggle emptyTog;
    public UIcontrol UiCtrl;
    private void Awake()
    {
        Debug.Log("Awake");
    }
    void Start()
    {


        InitTxts("Txt/上饶市城东水厂.txt");
        InitTxts("Txt/广丰水厂.txt");
        InitTxts("Txt/弋阳水厂.txt");
        InitTxts("Txt/横峰水厂.txt");
        InitTxts("Txt/玉山水厂.txt");
        InitTxts("Txt/大港桥水厂.txt");
        InitTxts("Txt/余干县城区水厂.txt");
        InitTxts("Txt/余江花园水厂.txt");
        InitTxts("Txt/婺源水厂.txt");
        InitTxts("Txt/德兴润泉水厂.txt");
        InitTxts("Txt/贵溪中心区水厂.txt");
        InitTxts("Txt/鄱阳水厂.txt");
        InitTxts("Txt/鄱阳第二水厂.txt");
        InitTxts("Txt/鹰潭江南水厂.txt");
        InitTxts("Txt/景德镇大石口河道.txt");
        InitTxts("Txt/景德镇樟树坑河道.txt");
        InitTxts("Txt/景德镇第四河道.txt");
        InitTxts("Txt/乐平共产主义水库.txt");
        InitTxts("Txt/乐平磻溪河道.txt");

        DownLoadInfo();

        InitPanel();
        Debug.Log("Start");
    }
    private void InitDayPanel()
    {
        int numLargeThen30 = 0;
        int numMid = 0;
        int numLittleThen15 = 0;  
        string URL = "http://service-18c85614-1251413122.gz.apigw.tencentcs.com/hyits/sr/share_water_source_today_8h?token=1234abcd4321";
        NetRequest._Instance.Net_Post(URL, (request) =>
        { 
            string json = request.downloadHandler.text;
            JsonData jd = new JsonData();
            jd = JsonMapper.ToObject(json)["data"];
            foreach (var value in List_CityPlace)
            {
                foreach (var placeNameTrans in value.place)
                {
                    string name = placeNameTrans.GetComponent<PanoramaDataController>().Ide;
                    if (Dic_PlaceInfo.ContainsKey(name))
                    {
                        PlaceData pd = Dic_PlaceInfo[name];
                        float kurong = float.Parse(pd.kurong.Replace("百万立方米", ""));
                        float sikuron = float.Parse(pd.sikurong.Replace("百万立方米", ""));
                        try
                        {

                            if (Dic_PlaceID.ContainsKey(name))
                            {
                                float INQ = 0;
                                float OTQ = 0;
                                string INQData = "";
                                for (int i = 0; i < jd.Count; i++)
                                {
                                    if (jd[i]["ID"].ToString() == Dic_PlaceID[name])
                                    {
                                        try
                                        {
                                            INQ = float.Parse(jd[i]["INQ"].ToString()) / 1000000;
                                            INQData = jd[i]["INQ"].ToString();
                                        }
                                        catch
                                        {
                                            INQ = 0;
                                        }
                                        try
                                        {

                                            OTQ = float.Parse(jd[i]["OTQ"].ToString()) / 1000000;
                                        }
                                        catch
                                        {
                                            OTQ = 0;
                                        }
                                        break;
                                    }
                                }
                                if (getWaterL(kurong, sikuron, OTQ, INQ, 30)) //水量大于三十天
                                {
                                    numLargeThen30++;
                                }
                                else if (getWaterL(kurong, sikuron, OTQ, INQ, 15))//水量大于十五天
                                {
                                    numMid++;
                                }
                                else //水量小于十五天
                                {
                                    numLittleThen15++;
                                }
                            }

                        }
                        catch
                        {
                            Debug.Log("未读取到data数据");
                        }
                    }
                }
            }
            Debug.Log("大于三十天："+numLargeThen30+"中间："+numMid+"小于十五天："+numLittleThen15);
            UiCtrl.showTxt(numLargeThen30,numMid,numLittleThen15);
        });

    }

    private void InitPanel()
    {
        int index = -1;
        foreach (var value in List_CityPlace)
        {
            index++;
            GameObject go = Instantiate(Item, Content);
            go.transform.Find("Toggle/Image/Label").GetComponent<Text>().text = value.City;
            GameObject ItemChild = go.transform.Find("Content/Toggle").gameObject;
            if (index == 0)
            {
                defaultToggle0 = go.transform.Find("Toggle").GetComponent<Toggle>();
            }
            foreach (var placeNameTrans in value.place)
            {
                index++;
                GameObject goChild = Instantiate(ItemChild, go.transform.Find("Content").transform);
                string name = placeNameTrans.GetComponent<PanoramaDataController>().Ide;
                goChild.transform.Find("Label").GetComponent<Text>().text = name;

                togglesAll.Add(name, goChild);
                goChild.GetComponent<Toggle>().onValueChanged.AddListener((s) =>
                {
                    if (s)
                    {
                        if (lastShowPanoramaToggle != null && lastShowPanoramaToggle == goChild.GetComponent<Toggle>())
                        {
                            if (lastShowPanorama != null)
                            {
                                if (lastShowPanorama.gameObject.activeSelf == true)
                                {
                                    return;
                                }
                            }
                        }
                        Debug.Log("更新点位数据");
                        resetPanelInfo();
                        if (pointsUI.activeSelf==true)
                        {
                            pointsUI.SetActive(false);
                            MouseL.enabled = true;
                        }
                        if (aniTor.gameObject.activeSelf == false)
                        { 
                            aniTor.gameObject.SetActive(true);
                        }
                        if (MouseL.transform.GetComponent<PlayerController>().enabled== true)
                        {
                            MouseL.transform.GetComponent<PlayerController>().enabled = false;
                            MouseL.transform.GetComponent<CameraController>().ResetPos();
                        }
                        panelExc.gameObject.SetActive(false);
                        infoPanel.parent.Find("Title/Text").GetComponent<Text>().text = name + "基本信息";
                        if (lastShowPanorama)
                        {
                            if(lastShowPanorama!= placeNameTrans)
                                lastShowPanorama.gameObject.SetActive(false);
                        }
                        if (lastShowPanoramaToggle)
                        {
                            if(lastShowPanoramaToggle != goChild.GetComponent<Toggle>())
                            {
                                lastShowPanoramaToggle.isOn = false;
                            }
                        }
                        placeNameTrans.gameObject.SetActive(true);
                        TitleText.text = name;
                        TitleText.gameObject.SetActive(true);
                        if (Dic_PlaceInfo.ContainsKey(name))
                        {
                            PlaceData pd = Dic_PlaceInfo[name];
                            infoPanel.Find("Grids").GetChild(0).GetChild(1).GetComponent<Text>().text = pd.qushuikoudibangaocheng;
                            infoPanel.Find("Grids").GetChild(1).GetChild(1).GetComponent<Text>().text = pd.fencenggaocheng;
                            //infoPanel.Find("Grids").GetChild(2).GetChild(1).GetComponent<Text>().text = pd.shishishuiwei;
                            //infoPanel.Find("Grids").GetChild(3).GetChild(1).GetComponent<Text>().text = pd.kurong;
                           
                            string shishishuiwei = "0";
                            string kurong = "0"; 
                            if (Dic_PlaceID.ContainsKey(name))
                            {
                                string id = Dic_PlaceID[name];
                                string url = "http://service-18c85614-1251413122.gz.apigw.tencentcs.com/hyits/sr/share_water_source_last_7days?id=" + id + "&token=1234abcd4321";
                                Debug.Log("获取点位信息："+ url);
                                NetRequest._Instance.Net_Post(url, (request) =>
                                {
                                    Debug.Log("获取点位信息回调完毕");
                                    string json = request.downloadHandler.text;
                                    JsonData jd = new JsonData();
                                    try
                                    {
                                        jd = JsonMapper.ToObject(json)["data"];

                                        JsonData jdChild = jd[jd.Count - 1];
                                        try
                                        {
                                            shishishuiwei = jdChild["Z"].ToString();
                                        } catch {  }
                                        try
                                        {
                                            kurong = jdChild["W"].ToString();
                                        } catch { }
                                    }
                                    catch
                                    {
                                        Debug.Log("未读取到data数据");
                                    }
                                    infoPanel.Find("Grids").GetChild(2).GetChild(1).GetComponent<Text>().text = shishishuiwei + "m";
                                    infoPanel.Find("Grids").GetChild(3).GetChild(1).GetComponent<Text>().text = kurong + "百万m³";
                                });
                            }
                            else
                            {
                                infoPanel.Find("Grids").GetChild(2).GetChild(1).GetComponent<Text>().text = shishishuiwei + "m";
                                infoPanel.Find("Grids").GetChild(3).GetChild(1).GetComponent<Text>().text = kurong + "百万m³";
                            }

                            infoPanel.Find("Grids").GetChild(4).GetChild(1).GetComponent<Text>().text = pd.sikurong;
                            infoPanel.Find("Grids").GetChild(5).GetChild(1).GetComponent<Text>().text = pd.dangqiankegongshuiliang;//计算？
                            infoPanel.Find("Grids").GetChild(6).GetChild(1).GetComponent<Text>().text = pd.rigongshuiliang;
                            infoPanel.Find("Grids").GetChild(7).GetChild(1).GetComponent<Text>().text = pd.riguanggaishuiliang;
                            infoPanel.Find("Grids").GetChild(8).GetChild(1).GetComponent<Text>().text = pd.shengtaixiaxieliuliang;
                            //infoPanel.Find("Grids").GetChild(9).GetChild(1).GetComponent<Text>().text = pd.rukuliuliang;
                            //infoPanel.Find("Grids").GetChild(10).GetChild(1).GetComponent<Text>().text = pd.sanrihou;
                            //infoPanel.Find("Grids").GetChild(11).GetChild(1).GetComponent<Text>().text = pd.liurihou;
                            //infoPanel.Find("Grids").GetChild(12).GetChild(1).GetComponent<Text>().text = pd.jiurihou;
                            infoPanel.Find("ImageJianyi").GetChild(1).GetComponent<Text>().text = pd.jianyi;
                            string URL = "http://service-18c85614-1251413122.gz.apigw.tencentcs.com/hyits/sr/share_water_source_today_8h?token=1234abcd4321";
                            Debug.Log("获取INQQTQ信息：" + URL);
                            NetRequest._Instance.Net_Post(URL, (request) =>
                            {
                                Debug.Log("获取INQQTQ回调完毕");
                                string json = request.downloadHandler.text;
                                JsonData jd = new JsonData();
                                try
                                {
                                    jd = JsonMapper.ToObject(json)["data"];

                                    if (Dic_PlaceID.ContainsKey(name))
                                    {
                                        float INQ = 0;
                                        float OTQ = 0;
                                        string INQData = "";
                                        for (int i = 0; i < jd.Count; i++)
                                        {
                                            if (jd[i]["ID"].ToString() == Dic_PlaceID[name])
                                            {
                                                try
                                                {
                                                    INQ = float.Parse(jd[i]["INQ"].ToString()) / 1000000;
                                                    INQData = jd[i]["INQ"].ToString();
                                                }
                                                catch 
                                                {
                                                    INQ = 0;
                                                }
                                                try
                                                {

                                                    OTQ = float.Parse(jd[i]["OTQ"].ToString()) / 1000000;
                                                }
                                                catch 
                                                {
                                                    OTQ = 0;
                                                }
                                                break;
                                            }
                                        }
                                        infoPanel.Find("Grids").GetChild(9).GetChild(1).GetComponent<Text>().text = INQData + "m³/s";
                                        infoPanel.Find("Grids").GetChild(10).GetChild(1).GetComponent<Text>().text = getWaterValue(float.Parse(pd.kurong.Replace("百万m³", "")), float.Parse(pd.sikurong.Replace("百万m³", "")), OTQ, INQ, 3) + "百万m³";
                                        infoPanel.Find("Grids").GetChild(11).GetChild(1).GetComponent<Text>().text = getWaterValue(float.Parse(pd.kurong.Replace("百万m³", "")), float.Parse(pd.sikurong.Replace("百万m³", "")), OTQ, INQ, 6) + "百万m³";
                                        infoPanel.Find("Grids").GetChild(12).GetChild(1).GetComponent<Text>().text = getWaterValue(float.Parse(pd.kurong.Replace("百万m³", "")), float.Parse(pd.sikurong.Replace("百万m³", "")), OTQ, INQ, 9) + "百万m³";
                                    }

                                }
                                catch
                                {
                                    Debug.Log("未读取到data数据");
                                }
                            });
                        }
                        else
                        {
                            resetPanelInfo();
                        }

                        Button btnShuiwei = infoPanel.transform.Find("Grids/ShuiWeiGuoCheng").GetComponent<Button>();
                        btnShuiwei.onClick.RemoveAllListeners();
                        btnShuiwei.onClick.AddListener(() =>
                        {
                            if (Dic_PlaceID.ContainsKey(name))
                            {
                                GetInfosAllByID(Dic_PlaceID[name], name);
                            }
                            else
                            {
                                Debug.Log("字典内无该key:    " + name);
                                updatePanelExc(null, "", "");
                            }
                        });

                        lastShowPanorama = placeNameTrans;
                        lastShowPanoramaToggle = goChild.GetComponent<Toggle>();
                    }

                });
                if (index == 1)
                {
                    defaultToggle = goChild.GetComponent<Toggle>();
                }
                goChild.SetActive(true);
            }

            go.SetActive(true);

            go.transform.Find("Toggle/Image").gameObject.SetActive(true);
        }
    }
    public void setLastPanorama(bool isOn)
    {
        if (isOn)
        {
            Map.gameObject.SetActive(false);
            if (lastShowPanorama)
            {
                if (lastShowPanorama.gameObject.activeSelf == true)
                {
                    lastShowPanorama = null;
                    aniTor.gameObject.SetActive(false);
                    TitleText.gameObject.SetActive(false);

                    pointsUI.SetActive(true);
                    MouseL.enabled = false;
                    MouseL.transform.GetComponent<PlayerController>().enabled = true;
                    MouseL.transform.GetComponent<CameraController>().ResetPos();
                }
            }
            else
            {
                aniTor.gameObject.SetActive(false);
                TitleText.gameObject.SetActive(false);

                pointsUI.SetActive(true);
                MouseL.enabled = false;
                MouseL.transform.GetComponent<PlayerController>().enabled = true;
                MouseL.transform.GetComponent<CameraController>().ResetPos();
               
                InitDayPanel();
                LayoutRebuilder.ForceRebuildLayoutImmediate(Content.GetComponent<RectTransform>());
               

            }
        }
        else
        {
            aniTor.gameObject.SetActive(false);
            TitleText.gameObject.SetActive(false);
            pointsUI.SetActive(false); 
            Map.gameObject.SetActive(true);
        }
        Debug.Log(isOn);
      
        //if(isOn)
        //{
        //    if (lastShowPanorama)
        //    {
        //        pointsUI.SetActive(false);
        //    }
        //    else
        //    {
        //        pointsUI.SetActive(true);
        //    }
        //}
        //else
        //{
        //    pointsUI.SetActive(false);
        //}
    }

    public void InitTxts(string url)
    {
        NetRequest._Instance.File_Post(url, (request) =>
        {
            try
            {
                string json = request.downloadHandler.text;
                JsonData jd = JsonMapper.ToObject(json);
                PlaceData pd = new PlaceData();
                JsonData jdChild = jd["data"];
                pd.qushuikoudibangaocheng = jdChild["水源地位置"].ToString();
                pd.fencenggaocheng = jdChild["分层取水口高程"].ToString().Replace("米", "m");
                pd.shishishuiwei = jdChild["实时水位"].ToString().Replace("米", "m");
                pd.kurong = jdChild["库容"].ToString().Replace("立方米", "m³");
                pd.sikurong = jdChild["死库容"].ToString().Replace("立方米", "m³");
                pd.dangqiankegongshuiliang = jdChild["当前可供水量"].ToString();
                pd.rigongshuiliang = jdChild["日供水量"].ToString().Replace("立方米", "m³");
                pd.riguanggaishuiliang = jdChild["日灌溉水量"].ToString().Replace("立方米", "m³");
                pd.shengtaixiaxieliuliang = jdChild["生态下泄流量"].ToString().Replace("立方米", "m³/s");
                pd.rukuliuliang = jdChild["入库流量"].ToString().Replace("立方米/秒", "m³/s"); ;
                pd.sanrihou = jdChild["3日后可供水量"].ToString();
                pd.liurihou = jdChild["6日后可供水量"].ToString();
                pd.jiurihou = jdChild["9日后可供水量"].ToString();
                pd.jianyi = jdChild["建议"].ToString();

                string name = url.Split('/')[url.Split('/').Length - 1].Split('.')[0];
                Dic_PlaceInfo.Add(name, pd);
                Debug.Log("TXT文件加载完成！");
            }
            catch
            {
                Debug.Log("读取TXT文件失败！");
            }
        });
    }


    public void DownLoadInfo()
    {
        NetRequest._Instance.Net_Post("http://service-18c85614-1251413122.gz.apigw.tencentcs.com/hyits/sr/share_water_source_group_1?token=1234abcd4321", (request) =>
        {
            string json = request.downloadHandler.text;
            JsonData jd = JsonMapper.ToObject(json);
            for (int i = 0; i < jd["data"].Count; i++)
            {
                JsonData jdChild = jd["data"][i];
                string key = jdChild["STNM"].ToString();
                if (key == "上饶市旭日水厂")
                {
                    key = "上饶市城东水厂";
                }
                Dic_PlaceID.Add(key, jdChild["ID"].ToString());
            }

            Debug.Log("ID信息加载完成！"); 
            //InitPanel();
        });
    }

    public void GetInfosAllByID(string id, string name)
    {
        string URL = "http://service-18c85614-1251413122.gz.apigw.tencentcs.com/hyits/sr/share_water_source_last_7days?id=" + id + "&token=1234abcd4321";
        NetRequest._Instance.Net_Post(URL, (request) =>
            {
                string json = request.downloadHandler.text;
                JsonData jd = new JsonData();
                try
                {
                    jd = JsonMapper.ToObject(json)["data"];
                }
                catch
                {
                    Debug.Log("未读取到data数据");
                }
                updatePanelExc(jd, name, id);
            });
    }

    private void updatePanelExc(JsonData jd, string name, string id)
    {
        if (id != "")
        {
            Application.ExternalCall("openpanorama", "http://47.104.197.199/charts/index.html?id=" + id);
            Debug.Log("ID" + id);
        }
        panelExc.gameObject.SetActive(true);
        panelExc.Find("Title/Text").GetComponent<Text>().text = name;
        if (jd == null || jd.Count == 0)
        {
            Debug.Log("暂无数据");
            return;
        }
        Debug.Log("------------------------------------------------------------" + jd.Count);
        jdExcel = jd;
        InitScroll(jd.Count);
    }

    private void InitScroll(int GridCount)
    {
        ScrollGridVertical scrollGridVertical = excPanelScrollView.gameObject.GetComponent<ScrollGridVertical>();
        if (scrollGridVertical)
        {
            //如果已经初始化过
            scrollGridVertical.SetCellCount(GridCount);
        }
        else
        {
            scrollGridVertical = excPanelScrollView.gameObject.AddComponent<ScrollGridVertical>();
            //步骤一：设置模板cell。
            scrollGridVertical.tempCell = exclItem;
            //步骤二:设置cell刷新的事件监听。
            scrollGridVertical.AddCellListener(this.OnCellUpdate);
            //步骤三：设置数据总数。
            //如果数据有新的变化，重新直接设置即可。
            scrollGridVertical.SetCellCount(GridCount);
        }

    }
    /// <summary>
    /// 监听cell的刷新消息，修改cell的数据。
    /// </summary>
    /// <param name="cell"></param>
    private void OnCellUpdate(ScrollGridCell cell)
    {
        JsonData jdChild = jdExcel[jdExcel.Count - 1 - cell.index];
        try
        {
            cell.transform.Find("Time/Text").GetComponent<Text>().text = jdChild["TM"].ToString().Replace("T", "  ").Replace("Z", " ");
        }
        catch
        {
            cell.transform.Find("Time/Text").GetComponent<Text>().text = "";
        }
        try
        {
            cell.transform.Find("W/Text").GetComponent<Text>().text = jdChild["Z"].ToString();
        }
        catch
        {

            cell.transform.Find("W/Text").GetComponent<Text>().text = "0";
        }
        try
        {
            cell.transform.Find("Z/Text").GetComponent<Text>().text = jdChild["W"].ToString();
        }
        catch
        {

            cell.transform.Find("Z/Text").GetComponent<Text>().text = "0";
        }
    }
    public void setAniBool(bool show)
    {
        aniTor.SetBool("Show", show);
    }

    public void OnSearch(string name)
    {
        Debug.Log("Search:" + name);
        if (name == null || name == "")
        {
            foreach (string key in togglesAll.Keys)
            {
                togglesAll[key].SetActive(true);
            }
            for (int i = 1; i < Content.childCount; i++)
            {
                Content.GetChild(i).Find("Toggle").GetComponent<Toggle>().isOn = false;
                Content.GetChild(i).Find("Toggle").GetComponent<Toggle>().onValueChanged.Invoke(false);
            }
        }
        else
        {
            List<Transform> goParents = new List<Transform>();
            foreach (string key in togglesAll.Keys)
            {
                if (key.Contains(name))
                {
                    togglesAll[key].SetActive(true);
                    if (!goParents.Contains(togglesAll[key].transform.parent.parent))
                        goParents.Add(togglesAll[key].transform.parent.parent);
                }
                else
                {
                    togglesAll[key].SetActive(false);
                }
            }
            for (int i = 1; i < Content.childCount; i++)
            {
                if (goParents.Contains(Content.GetChild(i)))
                {
                    Content.GetChild(i).Find("Toggle").GetComponent<Toggle>().isOn = true;
                    Content.GetChild(i).Find("Toggle").GetComponent<Toggle>().onValueChanged.Invoke(true);
                }
                else
                {
                    Content.GetChild(i).Find("Toggle").GetComponent<Toggle>().isOn = false;
                    Content.GetChild(i).Find("Toggle").GetComponent<Toggle>().onValueChanged.Invoke(false);
                }
            }

        }

    }
    public void OnCloseWebWindow()
    {
        Application.ExternalCall("closepanorama");
    }
    public string getWaterValue(float kurong, float sikurong, float chukuliuliang, float rukuliuliang, int tianshu)
    {
        string str = (kurong - sikurong - (chukuliuliang - rukuliuliang) * 3600 * 24 * tianshu).ToString("f3");
        while(str.EndsWith("0"))
        {
           str = str.Remove(str.Length - 1);
        }
        if (str.EndsWith("."))
        {
            str = str.Remove(str.Length - 1);
        }
        return str;
    }
    public bool getWaterL(float kurong, float sikurong, float chukuliuliang, float rukuliuliang, int tianshu)
    {
        return kurong - sikurong - (chukuliuliang - rukuliuliang) * 3600 * 24 * tianshu>0;
    }
    public void resetSelect()
    {
     
        if (lastShowPanoramaToggle != null)
        {
            emptyTog.isOn = true;
            lastShowPanoramaToggle.isOn = false; 
            lastShowPanoramaToggle = null;
        } 
        if (lastShowPanorama != null)
        {
            lastShowPanorama.gameObject.SetActive(false);
            lastShowPanorama = null; 
        }
        for (int i = 1; i < Content.childCount; i++)
        {
            Content.GetChild(i).Find("Toggle").GetComponent<Toggle>().isOn = false;
        }
    }


    public void OnSelect(string name)
    {
        Debug.Log("OnSelect:" + name);
        if (name == null || name == "")
        {
            foreach (string key in togglesAll.Keys)
            {
                togglesAll[key].SetActive(true);
            }
            for (int i = 1; i < Content.childCount; i++)
            {
                Content.GetChild(i).Find("Toggle").GetComponent<Toggle>().isOn = false;
                Content.GetChild(i).Find("Toggle").GetComponent<Toggle>().onValueChanged.Invoke(false);
            }
        }
        else
        {
            foreach (string key in togglesAll.Keys)
            {
                if (key==name)
                {
                    togglesAll[key].transform.parent.parent.GetChild(0).GetComponent<Toggle>().isOn = true;
                    togglesAll[key].transform.parent.parent.GetChild(0).GetComponent<Toggle>().onValueChanged.Invoke(true);
                    togglesAll[key].GetComponent<Toggle>().isOn = true;
                    Debug.Log("打开toggle：" + key +" ----"+ togglesAll[key].GetComponent<Toggle>().isOn);
                    togglesAll[key].GetComponent<Toggle>().onValueChanged.Invoke(true);
                }
            }
            for (int i = 1; i < Content.childCount; i++)
            {
                if (togglesAll.ContainsKey(name)&&Content.GetChild(i)== togglesAll[name].transform.parent.parent)
                {
                    Content.GetChild(i).Find("Toggle").GetComponent<Toggle>().isOn = true;
                    Content.GetChild(i).Find("Toggle").GetComponent<Toggle>().onValueChanged.Invoke(true);
                }
                else
                {
                    Content.GetChild(i).Find("Toggle").GetComponent<Toggle>().isOn = false;
                    Content.GetChild(i).Find("Toggle").GetComponent<Toggle>().onValueChanged.Invoke(false);
                }
            }
        }

    }
    public void resetPanelInfo()
    {
        infoPanel.Find("Grids").GetChild(0).GetChild(1).GetComponent<Text>().text ="";
        infoPanel.Find("Grids").GetChild(1).GetChild(1).GetComponent<Text>().text = "";
        infoPanel.Find("Grids").GetChild(2).GetChild(1).GetComponent<Text>().text = "";
        infoPanel.Find("Grids").GetChild(3).GetChild(1).GetComponent<Text>().text = "";
        infoPanel.Find("Grids").GetChild(4).GetChild(1).GetComponent<Text>().text = "";
        infoPanel.Find("Grids").GetChild(5).GetChild(1).GetComponent<Text>().text = "";
        infoPanel.Find("Grids").GetChild(6).GetChild(1).GetComponent<Text>().text ="";
        infoPanel.Find("Grids").GetChild(7).GetChild(1).GetComponent<Text>().text ="";
        infoPanel.Find("Grids").GetChild(8).GetChild(1).GetComponent<Text>().text = "";
        infoPanel.Find("Grids").GetChild(9).GetChild(1).GetComponent<Text>().text = "";
        infoPanel.Find("Grids").GetChild(10).GetChild(1).GetComponent<Text>().text = "";
        infoPanel.Find("Grids").GetChild(11).GetChild(1).GetComponent<Text>().text = "";
        infoPanel.Find("Grids").GetChild(12).GetChild(1).GetComponent<Text>().text = "";
    }
}
