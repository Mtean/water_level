﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommrntaryBackUp : MonoBehaviour
{
    public Animator ani;
    public string Aniname;
    /// <summary>
    /// 动画播放
    /// </summary>
    public void OnPlay()
    {
        ani.enabled = true;
        ani.Play(Aniname);
        if (Aniname == "Down")
            Aniname = "UP";
        else
            Aniname = "Down";

    }
    /// <summary>
    /// 初始化组件
    /// </summary>
    public void OnReset()
    {
        ani.Play("UP", 0, 0f);
        ani.Update(0f);
        Aniname = "UP";
        ani.enabled = false;
    }
    public void OnHide()
    {
        ani.Play("Up");
    }
    public void OnShow()
    {
        ani.Play("Back");
    }
}
