﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class BindToPoint : MonoBehaviour,IPointerClickHandler
{
    private HeadWatersPanel H;
    private Transform bindPoint;
    private string ID;
    private void Update()
    {
        transform.position = Camera.main.WorldToScreenPoint(bindPoint.position);
    }
    public void InitUI(Transform _bindPoint,HeadWatersPanel h)
    {
        H = h;
        bindPoint = _bindPoint;
        ID = bindPoint.GetComponent<pointData>()._ID;
        gameObject.name = bindPoint.name;
        transform.GetComponentInChildren<Text>().gameObject.AddComponent<ContentSizeFitter>().horizontalFit = ContentSizeFitter.FitMode.PreferredSize;
        string t = bindPoint.name.Replace("（", "\n（");
        transform.GetComponentInChildren<Text>().text = t;
    }
    private void OnMouseDown()
    {
        H.OnSelect(ID);
        Debug.Log("OnSelect:   " + ID);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        H.OnSelect(ID);
        Debug.Log("OnPointerClickOnSelect:   " + ID);
    }
}
