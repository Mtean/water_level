﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using LitJson;
/// <summary>
/// 数据请求脚本
/// </summary>
public class NetRequest : MonoBehaviour
{
    /// <summary>
    /// 静态实例化
    /// </summary>
    public static NetRequest _Instance;
    JsonData tmp;
    public void Awake()
    {
        _Instance = this;
    }
    private void Start()
    {
        //Net_Post("http://service-18c85614-1251413122.gz.apigw.tencentcs.com/hyits/sr/share_water_source_group_1?token=1234abcd4321");
       // File_Post("2.txt", (uwr) =>
        //{
        //    tmp = JsonMapper.ToObject(uwr.downloadHandler.text);
            //Debug.Log(tmp["data"]["基本信息"].ToString());
            //Debug.Log(tmp["data"]["县"].ToString());
            //Debug.Log(tmp["data"]["所在河段"].ToString());
            //Debug.Log(tmp["data"]["经纬度"].ToString());
            //Debug.Log(tmp["data"]["集水面积"].ToString());
            //Debug.Log(tmp["data"]["取水口高程"].ToString());
            //Debug.Log(tmp["data"]["无法取水口高程"].ToString());
            //Debug.Log(tmp["data"]["枯警水位"].ToString());
            //Debug.Log(tmp["data"]["日取水量"].ToString());
            //Debug.Log(tmp["data"]["取水方式"].ToString());
            //Debug.Log(tmp["data"]["供水人数"].ToString());
            //Debug.Log(tmp["data"]["水源类型"].ToString());
        //});

    }
    /// <summary>
    /// 文件夹读取
    /// </summary>
    public void File_Post(string URL, Action<UnityWebRequest> callback = null)
    {
        StartCoroutine(IEPost(Application.streamingAssetsPath + "/" + URL,callback));
    }
    /// <summary>
    /// 网络请求
    /// </summary>
    public void Net_Post(string URL, Action<UnityWebRequest> callback=null)
    {
        StartCoroutine(IEPost(URL,callback));
    }
    /// <summary>
    /// 数据请求
    /// </summary>
    /// <param name="Url"></param>
    /// <param name="callback"></param>
    /// <returns></returns>
    IEnumerator IEPost(string Url, Action<UnityWebRequest> callback=null)
    {
        UnityWebRequest request = UnityWebRequest.Get(Url);       
        yield return request.SendWebRequest();
        if (request.isDone)
        {
            callback?.Invoke(request);
            //Debug.Log(request.downloadHandler.text);
        }
    }
}
