﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using LitJson;

[System.Serializable]
public struct PanoramaData
{
    /// <summary>
    /// 编号
    /// </summary>
    //public string STCD;
    /// <summary>
    /// 名字
    /// </summary>
    public string STNM;
    /// <summary>
    /// 水位
    /// </summary>
    public string W;
    /// <summary>
    /// 容量
    /// </summary>
    public string Z;

}
public class PlaneController : MonoBehaviour
{
    /// <summary>
    /// 地图信息控制器
    /// </summary>
    public MapDatasController mapController;
    /// <summary>
    /// 全景图库
    /// </summary>
    public PanoramaDataController[] Panoramas;
    /// <summary>
    /// 全景图数据集合
    /// </summary>
    public List<PanoramaData> panoramaDatas;
    /// <summary>
    /// 水位图数据地址
    /// </summary>
    readonly string PanoramadataUrl= "http://service-18c85614-1251413122.gz.apigw.tencentcs.com/hyits/sr/share_water_source_group_1?token=1234abcd4321";
    JsonData PanoramaJD;
    // Start is called before the first frame update
    void Start()
    {
        //NetRequest._Instance.Net_Post(PanoramadataUrl, (uwr) =>
        //{
        //    PanoramaJD = JsonMapper.ToObject(uwr.downloadHandler.text);
        //    foreach (JsonData item in PanoramaJD["data"])
        //    {
        //        PanoramaData data = new PanoramaData();
        //        data.STNM = item["STNM"] != null ? data.W = item["STNM"].ToString() : data.STNM = "";
        //        data.W = item["W"] != null ? data.W = item["W"].ToString() : data.W = "";
        //        data.Z = item["Z"] != null ? data.Z = item["Z"].ToString() : data.Z = "";
        //        panoramaDatas.Add(data);
        //    }
        //    OnSetDataPanorama();
        //});
    }
    /// <summary>
    /// 全景图选择
    /// </summary>
    /// <param name="num"></param>
    public void OnValueChanged(int num)
    {
        CameraController._ICamera.OnInit();
        CameraController._ICamera.OnShowMouse(true);
        for (int i = 0; i < Panoramas.Length; i++)
        {
            if (i==num)
            {
                Panoramas[i].OnIsShow(true);
                continue;
            }
            Panoramas[i].OnIsShow(false);
        }       
    }
    /// <summary>
    /// 关闭全景图显示
    /// </summary>
    public void OnClose()
    {
        foreach (var item in Panoramas)
            item.gameObject.SetActive(false);
    }
    /// <summary>
    /// 信息设置
    /// </summary>
    public void OnSetDataPanorama()
    {
        for (int i = 0; i < panoramaDatas.Count; i++)
        {
            for (int j = 0; j < Panoramas.Length; j++)
            {
                if (Panoramas[j].Ide == panoramaDatas[i].STNM) 
                {
                    Panoramas[j].OnSetData(panoramaDatas[i]);
                    continue;
                }                  
            }
        }
    }
}
