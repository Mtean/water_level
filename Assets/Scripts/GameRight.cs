﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameRight : MonoBehaviour
{
    /// <summary>
    /// 速度
    /// </summary>
    public float Speed;
    /// <summary>
    /// 是否可以旋转
    /// </summary>
    bool IsRota = false;
    float MaxTime = 5;
    float InitTime = 0;
    private void Update()
    {
        //if (PlayerController.IsStar)
        //{
        //    IsRota = false;
        //    InitTime = 0;
        //}
        //if (!PlayerController.IsStar&&!IsRota)
        //{          
        //    InitTime += Time.deltaTime;
        //    if (InitTime>= MaxTime)
        //    {
        //        IsRota = true;
        //        InitTime = 0;
        //    }
        //}
    }
    // Update is called once per frame
    void LateUpdate()
    {
        if (IsRota)
        {
            transform.Rotate(Vector3.up * Speed * Time.deltaTime);
        }     
    }
}
