﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpriteChange : MonoBehaviour
{
    public Transform img;
    public Transform ChildContent;

    public RectTransform Item;
    public RectTransform ContentTop;

    public void ChangeSp(bool isOn)
    {
        if(isOn)
        {
            img.transform.rotation = new Quaternion(0, 0, 180, 0);
            Debug.Log("ChangeSpT");
        }
        else
        {
            img.transform.rotation = new Quaternion(0, 0, 0, 0);
            Debug.Log("ChangeSpF");
        }
    }

    public void ChangeChildContent(bool isOn)
    {
        if (isOn)
        {
            ChildContent.gameObject.SetActive(true); Debug.Log("ChangeChildContentT");
        }
        else
        {
            ChildContent.gameObject.SetActive(false); Debug.Log("ChangeChildContentF");
        }
        LayoutRebuilder.ForceRebuildLayoutImmediate(Item);
        LayoutRebuilder.ForceRebuildLayoutImmediate(ContentTop);
    }

}
