﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Jisuan : MonoBehaviour
{
    public InputField[] inputF;
    public Text Txtkeyongshuiliang;
    public Animation ani;
    void Start()
    {
        for(int i=0;i<inputF.Length;i++)
        {
            inputF[i].onValueChanged.AddListener((s) => {
                try
                {
                    float keyongshuiliang = float.Parse(inputF[0].text) - float.Parse(inputF[1].text) - (float.Parse(inputF[2].text) / 1000000 - float.Parse(inputF[3].text) / 1000000) * 3600 * 24 * float.Parse(inputF[4].text);
                    Txtkeyongshuiliang.text = keyongshuiliang.ToString();
                    if(keyongshuiliang<=0)
                    {
                        Txtkeyongshuiliang.color = Color.red;
                        ani.Play();
                    }
                    else
                    {
                        Txtkeyongshuiliang.color = Color.white;
                        ani.Stop();
                        Txtkeyongshuiliang.transform.localScale = Vector3.one;
                    }
                }
                catch
                {
                    Debug.Log("未知计算！");
                    Txtkeyongshuiliang.text = "";
                }
            });
        }
    }
}
