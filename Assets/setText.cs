﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class setText : MonoBehaviour
{
    public InputField inputTarget;
    public InputField[] inputfileds;
    public void setTargetText()
    {
        float num=0;
        foreach (var item in inputfileds)
        {
            string txt = item.text.ToString();
            if(txt=="")
            {
                txt = "0";
            }
            num += float.Parse(txt);
        }
        inputTarget.text = num.ToString();
    }
}
