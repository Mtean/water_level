﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using LitJson;
using UnityEngine.EventSystems;
/// <summary>
/// 地图点位信息
/// </summary>
public class MapPointData : MonoBehaviour
{
    public string IDs;
    /// <summary>
    /// 点位信息
    /// </summary>
    public Pointdata Pointdata;
    /// <summary>
    /// 位置
    /// </summary>
    public Vector3 pos3;
    /// <summary>
    /// 点击事件按钮
    /// </summary>
    Button btn;
    /// <summary>
    /// 信息面板树
    /// </summary>
    public WaterControllerPlane AniPlane;
    /// <summary>
    /// 聚集点位
    /// </summary>
    public GameObject Point;
    JsonData dataJD;
    // Start is called before the first frame update
    void Start()
    {
        pos3 = transform.position;
        NetRequest._Instance.File_Post(gameObject.name + ".txt", (uwr) =>
        {
            dataJD = JsonMapper.ToObject(uwr.downloadHandler.text);
            Pointdata.basicInfo = dataJD["data"]["基本信息"].ToString();
            IDs = Pointdata.basicInfo;
            Pointdata.reach = dataJD["data"]["所在河段"].ToString();
            Pointdata.waterType = dataJD["data"]["水源类型"].ToString();
            Pointdata.hydropsPlane = dataJD["data"]["集水面积"].ToString();
            Pointdata.intake = dataJD["data"]["取水口高程"].ToString();
            Pointdata.nointake = dataJD["data"]["无法取水口高程"].ToString();
            Pointdata.drywellLevel = dataJD["data"]["枯警水位"].ToString();
            Pointdata.waterIntakeMode = dataJD["data"]["取水方式"].ToString();
            Pointdata.designWaterScale = dataJD["data"]["设计供水规模"].ToString();
            Pointdata.dayIntake = dataJD["data"]["日取水量"].ToString() + "吨";
            Pointdata.waterPeopleNum = dataJD["data"]["供水人数"].ToString();
            Pointdata.city = dataJD["data"]["市"].ToString();
            Pointdata.county = dataJD["data"]["县"].ToString();
            Pointdata.managementUnit = dataJD["data"]["运行管理单位"].ToString();
            OnClassIfy();
        });
    }
    /// <summary>
    /// 市级分类
    /// </summary>
    public void OnClassIfy()
    {
        MapDatasController._Instance.OnAddPoint(this);
    }
    /// <summary>
    /// 信息设置
    /// </summary>
    public void OnSetData()
    {
        Point.transform.position = new Vector3(pos3.x, pos3.y + 0.7f, pos3.z - 0.01f);
        CameraController._ICamera.OnSetPos(pos3);
        AniPlane.OnInitAni(Pointdata);
    }
    /// <summary>
    /// 按下在松开时调用
    /// </summary>
    public void OnMouseUpAsButton()
    {
        if (!EventSystem.current.IsPointerOverGameObject())
        {
            OnSetData();
        }
    }
}
