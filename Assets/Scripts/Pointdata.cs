﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// 点位信息
/// </summary>
[System.Serializable]
public struct Pointdata
{
	/// <summary>
	/// 基本信息
	/// </summary>
	public string basicInfo;
	/// <summary>
	/// 所在河段
	/// </summary>
	public string reach;
	/// <summary>
	/// 水源类型
	/// </summary>
	public string waterType;
	/// <summary>
	/// 集水面积
	/// </summary>
	public string hydropsPlane;
	/// <summary>
	/// 取水口高程
	/// </summary>
	public string intake;
	/// <summary>
	/// 无法取水口高程
	/// </summary>
	public string nointake;
	/// <summary>
	/// 枯警水位
	/// </summary>
	public string drywellLevel;
	/// <summary>
	/// 取水方式
	/// </summary>
	public string waterIntakeMode;
	/// <summary>
	/// 设计供水规模
	/// </summary>
	public string designWaterScale;
	/// <summary>
	/// 日取水量
	/// </summary>
	public string dayIntake;
	/// <summary>
	/// 供水人数
	/// </summary>
	public string waterPeopleNum;
	/// <summary>
	/// 市
	/// </summary>
	public string city;
	/// <summary>
	/// 县
	/// </summary>
	public string county;
	/// <summary>
	/// 运行管理单位
	/// </summary>
	public string managementUnit;

}
/// <summary>
/// 市级枚举
/// </summary>
public enum CityType
{
	信州区,广信区,广丰区,玉山县,铅山县,弋阳县,横峰县,婺源县,德兴市,万年县,余干县,鄱阳县 ,鹰潭市,余江县,贵溪市
}
/// <summary>
/// 市级点集合
/// </summary>
[System.Serializable]
public struct CityWaterAll
{
	public CityType type;
	public List<MapPointData> mapPoints;
}
