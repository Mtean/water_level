﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// 角色控制器
/// </summary>
public class PlayerController : MonoBehaviour
{
    public float view_value;
    public void Update()
    {
        if (Input.GetMouseButton(1))
        {
            if (Camera.main.fieldOfView<=60)
            {
                transform.Translate(Vector3.left * Input.GetAxis("Mouse X"));
                transform.Translate(Vector3.down * Input.GetAxis("Mouse Y"));
            }
           
        }
        if (Input.GetAxis("Mouse ScrollWheel") != 0)
        {
            float fov = Camera.main.fieldOfView;
            fov -= Input.GetAxis("Mouse ScrollWheel") * view_value;
            fov = Mathf.Clamp(fov, 10, 60);
            Camera.main.fieldOfView = fov;
        }
    }
}
