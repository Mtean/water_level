﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// 相机插值移动控制器
/// </summary>
public class CameraController : MonoBehaviour
{
    /// <summary>
    /// 摄像机
    /// </summary>
     Camera Icamera;
    /// <summary>
    /// 控制脚本
    /// </summary>
    public static CameraController _ICamera;
    /// <summary>
    /// 操作脚本
    /// </summary>
     Mouselook mouselook;
    /// <summary>
    /// 平移位置
    /// </summary>
    Vector3 pos2;
    /// <summary>
    /// 最低视距
    /// </summary>
    public float lowestView;
    /// <summary>
    /// 是否开始
    /// </summary>
    bool IsPlay;
    /// <summary>
    /// 相差值
    /// </summary>
    float view;
    void Start()
    {
        _ICamera = this;
        Icamera = GetComponent<Camera>();
        mouselook = GetComponent<Mouselook>();
        OnShowMouse(false);
    }
    /// <summary>
    /// 初始化
    /// </summary>
    public void OnInit()
    {
        IsPlay = false;
        Icamera.transform.localPosition = Vector3.zero;
        Icamera.transform.localEulerAngles = Vector3.zero;      
        Icamera.fieldOfView = 60;
    }
    /// <summary>
    /// 控制脚本设置
    /// </summary>
    /// <param name="IsShow"></param>
    public void OnShowMouse(bool IsShow)
    {
        mouselook.enabled = IsShow;
    }
    /// <summary>
    /// 移动开始
    /// </summary>
    private void LateUpdate()
    {
        view = (pos2 - transform.position).magnitude;
        if (view>1&& IsPlay)
        {
            transform.position = Vector3.Lerp(transform.position, pos2, Time.deltaTime);
            Icamera.fieldOfView = Mathf.Lerp(Icamera.fieldOfView, lowestView, Time.deltaTime);
        }
        else 
            IsPlay = false;                 
    }
    /// <summary>
    /// 位置设置
    /// </summary>
    /// <param name="pos"></param>
    public void OnSetPos(Vector3 pos)
    {
        pos2 = new Vector3(pos.x,pos.y,transform.position.z);
        IsPlay = true;
    }

    public void ResetPos()
    {
        Icamera.transform.localPosition = Vector3.zero;
        Icamera.transform.localEulerAngles = Vector3.zero;
        Icamera.fieldOfView = 60;
    }
}
