﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 市级控制面板
/// </summary>
public class CityControllerPlane : MonoBehaviour
{
    /// <summary>
    /// 市级水位集合点
    /// </summary>
    public CityWaterAll cityWater;
    /// <summary>
    /// 最大高度
    /// </summary>
    public float MaxHeight;
    /// <summary>
    /// 最小高度
    /// </summary>
    public float MinHeight;
    /// <summary>
    /// 坐标组件
    /// </summary>
    public RectTransform rect;
    /// <summary>
    /// 自动对比组件
    /// </summary>
    public ContentSizeFitter sizeFitter;
    /// <summary>
    /// 列表组件
    /// </summary>
    public ScrollRect ViewScroll;
    /// <summary>
    /// 滑动块
    /// </summary>
    public Scrollbar BarBox;
    /// <summary>
    /// 水源按钮克隆无题
    /// </summary>
    public GameObject WaterDataClone;
    /// <summary>
    /// 各自数据点集合
    /// </summary>
    public List< GameObject> MapGameAll;
    // Start is called before the first frame update
    void Awake()
    {
        rect = GetComponent<RectTransform>();
        cityWater.mapPoints = new List<MapPointData>();
        MapGameAll = new List<GameObject>();
    }
    /// <summary>
    /// 添加MapPointData
    /// </summary>
    public  void AddMapPoint(MapPointData data)
    {
        cityWater.mapPoints.Add(data);
        GameObject game = Instantiate(WaterDataClone, WaterDataClone.transform.parent);
        game.SetActive(true);
        if (game.GetComponent<Button>()==null)
            game.AddComponent<Button>();
        game.GetComponent<Button>().onClick.AddListener(data.OnSetData);
        game.GetComponentInChildren<Text>().text = data.Pointdata.basicInfo+" "+data.gameObject.name;
        MapGameAll.Add(game);
    }
    /// <summary>
    /// 初始化
    /// </summary>
    public void OnInit()
    {
        sizeFitter.enabled = false;
        rect.sizeDelta = new Vector2(rect.sizeDelta.x, MaxHeight);
        ViewScroll.gameObject.SetActive(true);
        BarBox.value = 1;
        sizeFitter.enabled = true;
        sizeFitter.SetLayoutVertical();

    }
    /// <summary>
    /// 关闭
    /// </summary>
    public void OnClose()
    {
        rect.sizeDelta = new Vector2(rect.sizeDelta.x, MinHeight);
        ViewScroll.gameObject.SetActive(false);
    }
    /// <summary>
    /// 搜索显示
    /// </summary>
    public void OnSearch(string name)
    {
        int n=0;
        for (int i = 0; i < MapGameAll.Count; i++)
        {
            if (MapGameAll[i].GetComponentInChildren<Text>().text.Contains(name))
            {
                MapGameAll[i].gameObject.SetActive(true);
                n++;
                continue;
            }
            MapGameAll[i].gameObject.SetActive(false);
        }
        Debug.Log(gameObject.name);
        Debug.Log(n);
        if (n>0)
        {
            OnInit();
        }
        else
        {
            OnClose();
        }
    }
    /// <summary>
    /// 重置搜索显示
    /// </summary>
    public void OnRestChangedShow()
    {      
        for (int i = 0; i < MapGameAll.Count; i++)
            MapGameAll[i].gameObject.SetActive(true);
        OnClose();
    }
}
