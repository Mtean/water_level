﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// 数据控制器
/// </summary>
public class DataController : MonoBehaviour
{
    /// <summary>
    /// 数据面板
    /// </summary>
    public GameObject DataPlane;
    /// <summary>
    /// 控制数据面板
    /// </summary>
    public void OnCtrPlane(bool IsShow)
    {
        DataPlane.gameObject.SetActive(IsShow);
    }

}
