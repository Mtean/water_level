{
    "code":"200",
    "data":{
        "水源地位置":"",
        "分层取水口高程":"182米",
        "实时水位":"63米",
        "库容":"132百万立方米",
        "死库容":"78百万立方米",
        "当前可供水量":"",
        "日供水量":"200000立方米",
        "日灌溉水量":"200000立方米",
        "生态下泄流量":"12000立方米",
        "入库流量":"2",
        "3日后可供水量":"",
        "6日后可供水量":"",
        "9日后可供水量":"",
        "建议":""
    },
    "success":true,
    "message":"查询成功"
}